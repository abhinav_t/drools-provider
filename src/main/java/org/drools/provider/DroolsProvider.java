package org.drools.provider;

import org.drools.core.impl.InternalKnowledgeBase;
import org.drools.core.impl.KnowledgeBaseFactory;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderError;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.rules.runner.ruleengine.spi.RuleEngine;

import java.io.UnsupportedEncodingException;

public class DroolsProvider implements RuleEngine {

    public static final String DROOLS = "Drools Rule Engine";

    private final KnowledgeBuilder knowledgeBuilder;
    private InternalKnowledgeBase internalKnowledgeBase;

    public DroolsProvider() {
        this.knowledgeBuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
    }

    @Override
    public String getDescription() {
        return DROOLS;
    }

    @Override
    public void setupRules(String rules) {
        try {
            this.knowledgeBuilder.add(ResourceFactory.newByteArrayResource(rules.getBytes("utf-8")),
                    ResourceType.DRL);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        for (KnowledgeBuilderError error : this.knowledgeBuilder.getErrors()) {
            System.out.println(error);
        }
        this.internalKnowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
        this.internalKnowledgeBase.addPackages(this.knowledgeBuilder.getKnowledgePackages());
    }

    @Override
    public <T, U> void execute(T inputData, U result) {
        StatelessKieSession kieSession = this.internalKnowledgeBase.newStatelessKieSession();
        kieSession.setGlobal("result", result);
        kieSession.execute(inputData);
    }

}
